var ajaxFormDemo = (function () {
    function init() {

        var bar = $('.bar');
        var percent = $('.percent');
        var status = $('#status');

        $('form').ajaxForm({
            beforeSerialize: function () {
                status.empty();
                var percentVal = '0%';
                $(".progress-bar").addClass("notransition");
                bar.width(percentVal);
                $('input[type=submit]').prop('disabled', true)
                percent.html(percentVal);
            },
            uploadProgress: function (event, position, total, percentComplete) {
                $(".progress-bar").removeClass("notransition");
                var percentVal = percentComplete + '%';
                bar.css('width', percentVal)
                bar.html(percentVal);
            },
            success: function () {
                var percentVal = '100%';
                bar.css('width', percentVal)
                bar.html(percentVal);
            },
            complete: function (xhr) {
                status.html(xhr.responseText);
                $('input[type=submit]').prop('disabled', false)
            },
            clearForm: true
        });
    }
    return {
        init: init
    }
})();

$(document).ready(function () {
    ajaxFormDemo.init();
})